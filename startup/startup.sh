#!/bin/bash

#Yum statements we gotta get out of the way
sudo yum update -y
sudo yum install -y vim python3 epel-release wget libevent-devel ncurses-devel glibc-static git
sudo yum groupinstall -y "Development Tools"

#Installing Python3
sudo yum install -y https://centos7.iuscommunity.org/ius-release.rpm
sudo yum update-y
sudo yum install -y python36u python36u-libs python36u-devel python36u-pip

#Installing powerline-shell
pip3.6 install --upgrade pip
pip3.6 install powerline-shell

#Making a builds folder
mkdir ~/builds
cd ~/builds

#Tmux Installation and configuration
wget https://github.com/tmux/tmux/releases/download/2.9/tmux-2.9.tar.gz
tar xzf tmux-2.9.tar.gz 
cd tmux-2.9
./configure && make
sudo make install
cd ~
git clone https://github.com/gpakosz/.tmux.git
ln -s -f .tmux/.tmux.conf
cp .tmux/.tmux.conf.local .

#Copying Configuration Files 
cd ~/scripts/startup
mkdir ~/.vim
cp .vimrc ~/
cp .bashrc ~/
cp plugins.vim ~/.vim/
cp .tmux.conf.local ~/
