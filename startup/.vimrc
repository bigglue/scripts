"Install and run vim-plug on first run
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

so ~/.vim/plugins.vim

" enable syntax processing
syntax enable

" settings tabs/spaces for 4 spaces
set tabstop=4
set softtabstop=4

"setting stuff
set wildmenu
set cursorline
set number
set showmatch
set title
set visualbell
set noerrorbells
set nowrap
set ignorecase
set smartcase
set smarttab
set incsearch

"Detect .md files as markdown
autocmd BufNewFile,BufRead *.md set filetype=markdown

"Lightline
set laststatus=2
if !has('gui_running')
	  set t_Co=256
endif
set noshowmode
let g:lightline = {
  \   'colorscheme': 'landscape',
  \   'active': {
  \     'left':[ [ 'mode', 'paste' ],
  \              [ 'gitbranch', 'readonly', 'filename', 'modified' ]
  \     ]
  \   },
	\   'component': {
	\     'lineinfo': ' %3l:%-2v',
	\   },
  \   'component_function': {
  \     'gitbranch': 'fugitive#head',
  \   }
  \ }
let g:lightline.separator = {
	\   'left': '', 'right': ''
  \}
let g:lightline.subseparator = {
	\   'left': '', 'right': '' 
  \}
