This script installs the following things:
1) tmux 2.9 and all dependencies needed to build it
		- https://github.com/tmux/tmux
		- https://liyang85.github.io/how-to-install-the-latest-stable-tmux-on-centos7.html
2) Powerline-shell
		- https://github.com/b-ryan/powerline-shell
3) Various VIM Plugins
		a) https://github.com/itchyny/lightline.vim
		- https://github.com/mattn/emmet-vim
		- https://github.com/scrooloose/nerdtree
		- https://github.com/tpope/vim-eunuch
		- https://github.com/easymotion/vim-easymotion
		- https://github.com/tpope/vim-fugitive
		- https://github.com/sheerun/vim-polyglot
